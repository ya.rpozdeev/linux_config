#zstyle :omz:plugins:ssh-agent agent-forwarding on
unsetopt beep

export ZSH_THEME_GIT_PROMPT_PREFIX="[%{$fg_bold[red]%}±%{$reset_color%}%{$fg_bold[white]%}"

export PATH=$HOME/bin:/usr/local/bin:$PATH
export VAGRANT_DEFAULT_PROVIDER="parallels"
export ZSH="$HOME/.oh-my-zsh"
export PSQL="/Applications/Postgres.app/Contents/Versions/10/bin/"

ZSH_THEME="bureau"

CASE_SENSITIVE="true"
HIST_STAMPS="mm/dd/yyyy"

plugins=(
  git
  git-flow
  gitignore
  brew
  docker
  docker-compose
  docker-machine
  dotenv
  gitignore
  colorize
  pep8
  autopep8
  postgres
  vagrant
  osx
  zsh-navigation-tools
  zsh-syntax-highlighting
  zsh-autosuggestions
)

source $ZSH/oh-my-zsh.sh
#source ~/.iterm2_shell_integration.zsh

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
    export EDITOR='vim'
    export _1LEFT="%{$fg[red]%}[remote] $_USERNAME $_PATH"
else
    export EDITOR='vim'
fi

# User configuration
export MANPATH="/usr/local/man:$MANPATH"
export LANG=ru_RU.UTF-8

# ssh
 export SSH_KEY_PATH="~/.ssh/rsa_id"

# Aliases
 alias resourse="source ~/.zshrc"
 alias ohmyzsh="source ~/.oh-my-zsh"
 alias cdp="cd /Volumes/DOC"
 alias egit='vim ~/.gitconfig'
 alias evim='vim ~/.vimrc'
 alias egignore='vim .gitignore'
 alias ebash='vim ~/.bashrc'
 alias ezsh='vim ~/.zshrc'
 alias evagrant='vim ./Vagrantfile'
 alias psh='python -m SimpleHTTPServer 8000'
 alias du='du -h'
 alias df='df -h'

# NPM
 alias npmlsg='npm ls -g --depth=0'
 alias npmlsp='npm ls -p --depth=0'

# Docker
 alias dockercrm='docker container rm $(docker container ls -aq) -f'
 alias dockerirm='docker image rm $(docker images -aq) -f'
 alias dcrestart='docker-compose kill & docker-compose rm -f & docker-compose build & docker-compose up'
 alias dockercls='docker container ls -a'
 alias dockerils='docker images'
 alias dockervls='docker volume list'
 alias dockervrm='docker volume rm $(docker volume ls -q) -f'

# PyCharm
 alias pycharm='open -a PyCharm'

# CHEAT.SH
fpath=(~/.zsh.d/ $fpath)
