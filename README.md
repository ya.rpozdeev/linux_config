## Конфигурационные файлы окружения профиля.

.zshrc - настройки zsh shell  
.vimrc - настройки редактора vim  
cht.sh - расширения (cheat.sh)[https://github.com/chubin/cheat.sh]  
kick_start.sh - скрипт инициализации окружения.  

### Использование

```bash
chmod +x ./linux_conf/kick_start.sh
./linux_conf/kick_start.sh
```

