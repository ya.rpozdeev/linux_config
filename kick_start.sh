#/bin/bash

sudo yum -y install zsh vim

cd $HOME
git clone https://gitlab.com/ya.rpozdeev/linux_config.git $HOME/

cp ./linux_config/.vimrc $HOME/
cp ./linux_config/.zshrc $HOME/
cp -r ./linux_config/.zsh.d $HOME/

sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

git clone https://github.com/VundleVim/Vundle.vim.git $HOME/.vim/bundle/Vundle.vim
vim +PluginInstall +qall

sudo chsh -s /bin/zsh $USER
zsh
